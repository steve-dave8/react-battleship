const fleet = [
    {
        type: "patrol-boat",
        size: 2,
        orientation: "landscape",
        health: 2,
        deployed: false
    },
    {
        type: "submarine",
        size: 3,
        orientation: "landscape",
        health: 3,
        deployed: false
    },
    {
        type: "battleship",
        size: 4,
        orientation: "landscape",
        health: 4,
        deployed: false
    },
    {
        type: "carrier",
        size: 5,
        orientation: "landscape",
        health: 5,
        deployed: false
    }
];

const buildShip = (size, placed = false) => {
    let ship = [];
    for (let i = 0; i < size; i++){
        ship.push(<div key={`cell-${i + 1}-of-${size}`} className={placed ? 'occupied__cell' : 'ship__cell'} data-cell={i}></div>);
    }
    return ship;
};

export { fleet, buildShip };