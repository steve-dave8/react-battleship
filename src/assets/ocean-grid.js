const rows = ["A", "B", "C", "D", "E", "F", "G", "H", "I"];

let oceanGrid = [];

rows.forEach((row, index) => {
    for (let i = 1; i <= 9; i++){
        oceanGrid.push({
            coord: `${row}${i}`,
            row: index + 1,
            column: i,
            occupantID: null,
            occupant: null,
            struck: false
        });
    }
});

const setColLabels = () => {
    let colLabels = [];
    for (let i = 0; i < 9; i++) {
        colLabels.push(<div key={`column-${i + 1}`}>{i + 1}</div>);
    }
    return colLabels;
};

export { rows, oceanGrid, setColLabels };