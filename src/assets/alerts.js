import Swal from "sweetalert2";
import { formatShipTitle } from "../helpers/formatTitle";

const alertSunk = Swal.mixin({
    position: "bottom",
    confirmButtonColor: "#0073e6",
    customClass: {
        popup: "alert-sunk",
        title: "alert-sunk",
        confirmButton: "alert-sunk"
    }
});

const alertEnemySunk = (shipType) => {
    alertSunk.fire({
        title: `You have sunk the enemy's ${formatShipTitle(shipType)}!`,
        timer: 2500
    });
};

const alertPlayerSunk = (shipType) => {
    alertSunk.fire({
        title: `The enemy has sunk your ${formatShipTitle(shipType)}!`,
        timer: 2500
    });
};

const alertLose = () => {
    alertSunk.fire({
        title: "Game Over! The enemy has sunk all of your ships."
    });
};

const alertWin = () => {
    alertSunk.fire({
        title: "Congratulations, you won! You sunk all enemy ships."
    });
};

const alertHardLose = () => {
    alertSunk.fire({
        title: "Game Over! You ran out of ammo."
    });
};

export { alertEnemySunk, alertPlayerSunk, alertLose, alertWin, alertHardLose };