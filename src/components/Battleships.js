import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { removeShipFromGrid } from '../state/player/playerGridSlice'
import { rotateAllShips, rotateOneShip, returnShip } from '../state/player/playerShipsSlice'
//UI components:
import Tooltip from '@mui/material/Tooltip'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRotateRight } from '@fortawesome/free-solid-svg-icons'
//Style:
import "../styles/battleships.scss"
//Asset imports:
import { buildShip } from '../assets/fleet'
import { formatShipTitle } from '../helpers/formatTitle'

const Battleships = (props) => {
    const playerShips = useSelector(state => state.playerShips)

    const dispatch = useDispatch()

    //Moving ships from marina to grid:
    const handleDragStart = (e, ship) => {
        setTimeout(() => {e.target.classList.add('hidden')}, 0)
        const shipRef = {...ship}
        delete shipRef.health
        delete shipRef.deployed
        props.setDraggedShip(shipRef)
    }

    const handleDragEnd = (e, shipType) => {
        const draggedShip = playerShips.find(ship => ship.type === shipType)
        if (!draggedShip.deployed) {
            e.target.classList.remove('hidden')
        }
    }

    const handleTouchStart = (e, ship) => {
        e.preventDefault()
        const shipStyle = e.target.parentElement.style
        shipStyle.width = e.target.parentElement.clientWidth + "px"
        shipStyle.position = "absolute"
        const shipRef = {...ship}
        delete shipRef.health
        delete shipRef.deployed
        props.setDragOrigin(parseInt(e.target.dataset.cell, 10))
        props.setDraggedShip(shipRef)
        props.setTouchMoving(true)
    }

    const handleTouchMove = (e) => {
        const shipStyle = e.target.parentElement.style
        shipStyle.left = e.changedTouches[0].clientX - e.target.offsetLeft - e.target.clientWidth/2 + "px"
        shipStyle.top = e.changedTouches[0].clientY - e.target.offsetTop - e.target.clientHeight/2 + "px"
    }

    const handleTouchEnd = (e) => {
        e.preventDefault()
        const shipStyle = e.target.parentElement.style
        shipStyle.width = null
        shipStyle.position = null
        shipStyle.left = null
        shipStyle.top = null

        const dropTarget = document.elementFromPoint(e.changedTouches[0].clientX, e.changedTouches[0].clientY)
        if (dropTarget.parentElement.id === "player-grid"){
            const row = parseInt(dropTarget.dataset.row, 10)
            const column = parseInt(dropTarget.dataset.column, 10)
            props.setTouchDropTarget({row, column})
        }
        props.setTouchMoving(false)
    }
    //-----------------------------------------

    //Moving ships from grid back to marina:
    const handleDragOver = (e) => {
        const draggedShip = playerShips.find(ship => ship.type === props.draggedShip.type)
        if (draggedShip.deployed) e.preventDefault() //only enable dropping if the ship was placed on the grid
    }

    const handleDrop = () => {
        const { type } = props.draggedShip
        //update player grid:
        dispatch(removeShipFromGrid(type))

        //mark ship as not deployed:
        dispatch(returnShip(type))

        //clear dragged ship selection:
        props.setDraggedShip({})
    }
    //-----------------------------------------

    useEffect(() => {
        if (props.touchMoving || (!props.touchMoving && !props.touchDropTarget.hasOwnProperty("marina"))) return
        handleDrop()
        props.setTouchDropTarget({})
    // eslint-disable-next-line
    }, [props.touchMoving, props.touchDropTarget])

    return (
        <section id="marina">
            <div id="rotate-all">
                <button type="button" onClick={() => dispatch(rotateAllShips())}>Rotate All</button>
            </div>
            <div className="battleship-tray" onDragOver={handleDragOver} onDrop={handleDrop}>
                {playerShips
                    .map(ship => { return (
                        <div key={ship.type}>
                            <div className={`ship-port ${ship.type}`}>
                                {!ship.deployed &&
                                    <div draggable="true" className={`draggable ship ship--${ship.orientation}`}
                                        onDragStart={(e) => handleDragStart(e, ship)}
                                        onMouseDown={(e) => props.setDragOrigin(parseInt(e.target.dataset.cell, 10))}
                                        onDragEnd={(e) => handleDragEnd(e, ship.type)}
                                        onTouchStart={(e) => handleTouchStart(e, ship)}
                                        onTouchMove={handleTouchMove}
                                        onTouchEnd={handleTouchEnd}
                                    >
                                        {buildShip(ship.size)}
                                    </div>
                                }
                            </div>
                            <div className={`rotate-one ${ship.deployed ? "invisible" : ""}`}>
                                <Tooltip 
                                    arrow placement="top"
                                    title={<span style={{fontSize: "1.2rem"}}>Rotate {formatShipTitle(ship.type)}</span>}
                                >
                                    <span>
                                        <FontAwesomeIcon
                                            icon={faArrowRotateRight}
                                            className="rotate-one__arrow"
                                            onClick={() => dispatch(rotateOneShip(ship.type))}
                                        />
                                    </span>
                                </Tooltip>
                            </div>
                        </div>                     
                    )})
                }
            </div>
        </section>
    )
}

export default Battleships