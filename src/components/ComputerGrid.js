import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { toggleGameOver } from '../state/game-status/gameOverSlice'
import { toggleTurn } from '../state/game-status/playerTurnSlice'
import { assignPlayerStatus } from '../state/game-status/playerStatusSlice'
import { incrementPlayerNotify } from '../state/game-status/playerNotificationsSlice'
import { decrementPlayerShots, assignPlayerShots } from '../state/salvo/playerShotsLeftSlice'
import { incrementCompSalvo, decrementCompSalvo } from '../state/salvo/maxCompSalvoSlice'
import { assignCompShots } from '../state/salvo/compShotsLeftSlice'
//Asset imports:
import { fleet } from '../assets/fleet'
import { rows, setColLabels, oceanGrid } from '../assets/ocean-grid'
import { shuffle } from '../helpers/shuffle'
import { formatShipTitle } from '../helpers/formatTitle'
import { alertEnemySunk, alertWin, alertHardLose } from '../assets/alerts'

//randomly rotate computer ships:
const compFleet = fleet.map(ship => {
    const shipUpdate = {...ship}
    delete shipUpdate.deployed
    if (Math.random() < 0.5) shipUpdate.orientation = "portrait"
    return shipUpdate
})

const ComputerGrid = () => {
    const [computerShips, setComputerShips] = useState(compFleet)  
    const [computerGrid, setComputerGrid] = useState(oceanGrid)
    //game status:
    const started = useSelector(state => state.started)
    const gameOver = useSelector(state => state.gameOver)
    const playerTurn = useSelector(state => state.playerTurn)
    const playerNotifications = useSelector(state => state.playerNotifications)
    //game settings:
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)
    const notifyShipHit = useSelector(state => state.notifyShipHit)
    const alertSunk = useSelector(state => state.alertSunk)
    //salvo states:
    const maxPlayerSalvo = useSelector(state => state.maxPlayerSalvo)
    const playerShotsLeft = useSelector(state => state.playerShotsLeft)
    const maxCompSalvo = useSelector(state => state.maxCompSalvo)

    const dispatch = useDispatch()

    //place ships on computer grid:
    useEffect(() => {
        let compGridUpdate = computerGrid.map(cell => { return {...cell}})
        let compGridDeck = computerGrid.map(cell => { return {...cell}})
        for (let index = computerShips.length - 1; index >= 0; index--) { //easier to place larger ships first
            let ship = computerShips[index]
            compGridDeck = shuffle(compGridDeck)
            let targetIndex = Math.floor(Math.random()*compGridDeck.length)
            let targetCell = compGridDeck[targetIndex]
            let targetRows = []
            let targetCols = []
            let targetColStart = targetCell.column
            let targetRowStart = targetCell.row

            //force ship to fit on grid:
            if (targetColStart + ship.size > 9) {
                targetColStart = 9 - ship.size + 1
            }
            if (targetRowStart + ship.size > 9) {
                targetRowStart = 9 - ship.size + 1
            }

            //get targeted rows and columns:
            if (ship.orientation === "landscape") {
                targetRows.push(targetRowStart)
                for (let i = 0; i < ship.size; i++){
                    targetCols.push(targetColStart + i)
                }
            } else if (ship.orientation === "portrait") {
                targetCols.push(targetColStart)
                for (let i = 0; i < ship.size; i++){
                    targetRows.push(targetRowStart + i)
                }
            }

            //check if any of the targeted cells are already occupied:
            const targetCells = compGridUpdate.filter(cell => targetRows.includes(cell.row) && targetCols.includes(cell.column))
            if (targetCells.some(cell => cell.occupantID)) {
                index++
                continue
            } 

            //update computer grid:
            compGridUpdate = compGridUpdate.map(cell => {
                if (targetRows.includes(cell.row) && targetCols.includes(cell.column)) {
                    const cellUpdate = {...cell}
                    cellUpdate.occupantID = ship.type
                    return cellUpdate
                }
                return cell
            })

            //remove targeted cells to prevent selecting them again:
            compGridDeck = compGridDeck.filter(cell => {
                if (targetRows.includes(cell.row) && targetCols.includes(cell.column)) {
                    return false
                } 
                return true
            })         
        }
        setComputerGrid(compGridUpdate)
    // eslint-disable-next-line
    }, [])

    const fireMissile = (cellID, struck) => {
        if (!started || !playerTurn || struck || gameOver) return

        //update computer grid:
        const targetCell = computerGrid.findIndex(cell => cell.coord === cellID)
        const cellUpdate = {...computerGrid[targetCell]}
        cellUpdate.struck = true     
        setComputerGrid(prev => [...prev.slice(0, targetCell), cellUpdate, ...prev.slice(targetCell + 1)])

        //update computer ships:
        const occupant = cellUpdate.occupantID
        let shipUpdate
        let sunk = false
        let totalSunk = 0
        if (occupant) {
            const targetShip = computerShips.findIndex(ship => ship.type === occupant)
            shipUpdate = {...computerShips[targetShip]}
            shipUpdate.health = shipUpdate.health - 1
            sunk = shipUpdate.health === 0
            if (sunk) totalSunk++ //the state update doesn't immediately apply so I can't use Array.every to check if all ships have been sunk.
            setComputerShips(prev => [...prev.slice(0, targetShip), shipUpdate, ...prev.slice(targetShip + 1)])
        }

        //update game status:
        const statusUpdate = occupant ? `hit ${notifyShipHit ? formatShipTitle(shipUpdate.type) : ""}` : "miss"
        dispatch(assignPlayerStatus(statusUpdate))
        dispatch(incrementPlayerNotify())
        if (version === "Salvo" || version === "Revenge"){
            if (playerShotsLeft - 1 === 0){
                dispatch(toggleTurn())
                dispatch(assignPlayerShots(maxPlayerSalvo))
            } else {
                dispatch(decrementPlayerShots())
            }
            if (sunk) {
                let updatedCompSalvo
                if (version === "Salvo"){
                    updatedCompSalvo = maxCompSalvo - 1
                    dispatch(decrementCompSalvo())
                    
                } else if (version === "Revenge"){
                    updatedCompSalvo = maxCompSalvo + 1
                    dispatch(incrementCompSalvo())
                }
                dispatch(assignCompShots(updatedCompSalvo))
            }
        } else {
            dispatch(toggleTurn())
        }

        //send alerts:
        if (sunk) {
            computerShips.forEach(ship => {
                if (!ship.health) totalSunk++
            })
            if (totalSunk === computerShips.length) {
                dispatch(toggleGameOver())
                //use timeout to make sure state updates get applied first
                setTimeout(alertWin, 0)
            } else {
                alertSunk && setTimeout(() => {alertEnemySunk(shipUpdate.type)}, 0)
            }
        } 
        else if (difficulty === "hard" && playerNotifications + 1 === 35){
            dispatch(toggleGameOver())
            setTimeout(alertHardLose, 0)
        }
    }

    return (
        <div className="grid-wrapper">
            <h2>Computer Grid</h2>
            <div></div>
            <div className="column-labels">
                {setColLabels()}
            </div>
            <div></div>
            <div id="computer-grid">
                {computerGrid
                    .map(cell => { return (
                        <div key={cell.coord} onClick={() => fireMissile(cell.coord, cell.struck)}>
                            {cell.struck &&
                                <div className={cell.occupantID ? "hit" : "miss"}></div>
                            }
                        </div>
                    )})
                }
            </div>
            <div className="row-labels">
                {rows.map(label => { return (
                    <div key={`computer-row-${label}`}>{label}</div>
                )})}
            </div>
        </div>
    )
}

export default ComputerGrid