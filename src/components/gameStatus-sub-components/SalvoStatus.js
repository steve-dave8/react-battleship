import React from 'react'
import { useSelector } from 'react-redux'

const SalvoStatus = () => {
    const maxPlayerSalvo = useSelector(state => state.maxPlayerSalvo)
    const maxCompSalvo = useSelector(state => state.maxCompSalvo)
    const playerShotsLeft = useSelector(state => state.playerShotsLeft)
    const compShotsLeft = useSelector(state => state.compShotsLeft)

    return (
        <p>
            Player's remaining salvo: 
            <span style={{fontWeight: "bold"}}> {playerShotsLeft}/{maxPlayerSalvo}</span>
            <span style={{margin: "0 15px"}}>||</span>
            Computer's remaining salvo: 
            <span style={{fontWeight: "bold"}}> {compShotsLeft}/{maxCompSalvo}</span>
        </p>
    )
}

export default SalvoStatus