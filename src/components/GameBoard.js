import React from 'react'
import "../styles/gameboard.scss"
//Component imports:
import PlayerGrid from './PlayerGrid'
import ComputerGrid from './ComputerGrid'

const GameBoard = (props) => {
    return (
        <section id="gameboard">
            <PlayerGrid
                draggedShip={props.draggedShip}
                setDraggedShip={props.setDraggedShip}
                dragOrigin={props.dragOrigin}
                setDragOrigin={props.setDragOrigin}
                touchMoving={props.touchMoving}
                setTouchMoving={props.setTouchMoving}
                touchDropTarget={props.touchDropTarget}
                setTouchDropTarget={props.setTouchDropTarget}
            />
            <ComputerGrid/>
        </section>
    )
}

export default GameBoard