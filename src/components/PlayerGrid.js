import React, { useEffect, useState, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { toggleGameOver } from '../state/game-status/gameOverSlice'
import { toggleTurn } from '../state/game-status/playerTurnSlice'
import { assignCompStatus } from '../state/game-status/compStatusSlice'
import { incrementCompNotify } from '../state/game-status/computerNotificationsSlice'
import { addShipToGrid, strikeCell } from '../state/player/playerGridSlice'
import { deployShip, reduceHealth } from '../state/player/playerShipsSlice'
import { incrementPlayerSalvo, decrementPlayerSalvo } from '../state/salvo/maxPlayerSalvoSlice'
import { assignPlayerShots } from '../state/salvo/playerShotsLeftSlice'
import { decrementCompShots, assignCompShots } from '../state/salvo/compShotsLeftSlice'
//Asset imports:
import { rows, setColLabels, oceanGrid } from '../assets/ocean-grid'
import { buildShip } from '../assets/fleet'
import { shuffle } from '../helpers/shuffle'
import { formatShipTitle } from '../helpers/formatTitle'
import { alertPlayerSunk, alertLose } from '../assets/alerts'

const targetCells = oceanGrid.map(cell => cell.coord)

/*Sample item in honing deck:
{
    type: "battleship",
    orientation: undefined,
    nearbyCells: [{ coord: "A2", row: 1, column: 2 }, { coord: "B2", row: 2, column: 1 }]
}
*/

const PlayerGrid = (props) => {
    const [targetDeck, setTargetDeck] = useState(targetCells)
    const [honingDeck, setHoningDeck] = useState([])
    /*State updates to the redux store are done synchronously whereas with react's useState hook state updates are async.
    Triggering the useEffect of the computer firing another salvo shot based on changes to the compShotsLeft state 
    (in redux store) causes the next shot to be fired before local state hooks (targetDeck and honingDeck) have finished 
    updating. (At least that's what I think the cause of the problem is. Another possibility is how the updates get
    batched together). A simple fix is to add a local state which upon being incremented triggers the effect.*/
    const [compSalvoFired, setCompSalvoFired] = useState(0)

    const lastHit = useRef()

    //game status:
    const started = useSelector(state => state.started)
    const gameOver = useSelector(state => state.gameOver)
    const playerTurn = useSelector(state => state.playerTurn)
    //game settings:
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)
    //player states:
    const playerGrid = useSelector(state => state.playerGrid)
    const playerShips = useSelector(state => state.playerShips)
    //salvo states:
    const maxPlayerSalvo = useSelector(state => state.maxPlayerSalvo)
    const maxCompSalvo = useSelector(state => state.maxCompSalvo)
    const compShotsLeft = useSelector(state => state.compShotsLeft)

    const dispatch = useDispatch()

    const handleDrop = (e, touchTarget = null) => {
        let targetRow, targetCol
        if (touchTarget){
            targetRow = touchTarget.row
            targetCol = touchTarget.column
        } else {
            targetRow = parseInt(e.target.dataset.row, 10)
            targetCol = parseInt(e.target.dataset.column, 10)
        }
        const { orientation, size, type } = props.draggedShip
        let targetRows = []
        let targetCols = []
        let targetColStart = targetCol
        let targetRowStart = targetRow

        //get targeted rows and columns:
        if (orientation === "landscape") {
            targetRows.push(targetRow)
            targetColStart = targetCol - props.dragOrigin
            for (let i = 0; i < size; i++){
                targetCols.push(targetColStart + i)
            }
        } else if (orientation === "portrait") {
            targetCols.push(targetCol)
            targetRowStart = targetRow - props.dragOrigin
            for (let i = 0; i < size; i++){
                targetRows.push(targetRowStart + i)
            }
        }

        //check if all ship cells will fit on the grid:
        if (targetColStart < 1 || targetRowStart < 1 || targetCols[targetCols.length - 1] > 9 || targetRows[targetRows.length - 1] > 9) {
            props.setDraggedShip({})
            return
        }

        //check if any of the targeted cells are already occupied:
        const targetCells = playerGrid.filter(cell => targetRows.includes(cell.row) && targetCols.includes(cell.column))
        if (targetCells.some(cell => cell.occupantID)) {
            props.setDraggedShip({})
            return
        } 

        //update player grid:
        const gridPayload = { targetRows, targetCols, draggedShip: props.draggedShip }
        dispatch(addShipToGrid(gridPayload))

        //mark ship as deployed:
        dispatch(deployShip(type))

        //clear dragged ship selection:
        props.setDraggedShip({})
    }

    const handleDragOver = (e) => {
        if (e.target.id !== "player-grid") e.preventDefault() //enable dropping unless mouse is directly over a grid gap
    }

    //Moving ships from grid back to marina:
    const handleTouchStart = (e, ship) => {
        if (started) return
        e.preventDefault()
        const shipStyle = e.target.parentElement.style
        shipStyle.width = e.target.parentElement.clientWidth + "px"
        shipStyle.height = e.target.parentElement.clientHeight + "px"
        shipStyle.position = "fixed"
        props.setDragOrigin(parseInt(e.target.dataset.cell, 10))
        props.setDraggedShip(ship)
        props.setTouchMoving(true)
    }

    const handleTouchMove = (e) => {
        if (started) return
        const shipStyle = e.target.parentElement.style
        shipStyle.left = e.changedTouches[0].clientX - e.target.offsetLeft - e.target.clientWidth/2 + "px"
        shipStyle.top = e.changedTouches[0].clientY - e.target.offsetTop - e.target.clientHeight/2 + "px"
    }

    const handleTouchEnd = (e) => {
        if (started) return
        e.preventDefault()
        const shipStyle = e.target.parentElement.style
        shipStyle.width = null
        shipStyle.height = null
        shipStyle.position = null
        shipStyle.left = null
        shipStyle.top = null

        const dropTarget = document.elementFromPoint(e.changedTouches[0].clientX, e.changedTouches[0].clientY)
        if (dropTarget.parentElement.id === "player-grid"){
            const row = parseInt(dropTarget.dataset.row, 10)
            const column = parseInt(dropTarget.dataset.column, 10)
            props.setTouchDropTarget({row, column})
        } else if (dropTarget.classList.contains("battleship-tray") || dropTarget.parentElement.classList.contains("battleship-tray") || dropTarget.classList.contains("ship-port")){
            props.setTouchDropTarget({marina: true})
        }
        props.setTouchMoving(false)
    }
    //-----------------------------------------

    useEffect(() => {
        if (props.touchMoving || (!props.touchMoving && !props.touchDropTarget.hasOwnProperty("row"))) return
        handleDrop(null, props.touchDropTarget)
        props.setTouchDropTarget({})
    // eslint-disable-next-line
    }, [props.touchMoving, props.touchDropTarget])

    useEffect(() => {
        if (!playerTurn && !gameOver) {
            setTimeout(() => {
                //computer picks a random coordinate on the player grid:
                let targetDeckCopy = [...targetDeck]
                let targetIndex, targetCoord, honingCellsCopy
                if ((difficulty === "normal" || difficulty === "hard") && honingDeck.length) {
                    honingCellsCopy = [...honingDeck[0].nearbyCells]
                    targetIndex = Math.floor(Math.random()*honingCellsCopy.length)
                    targetCoord = honingCellsCopy[targetIndex].coord
                } else {
                    targetDeckCopy = shuffle(targetDeckCopy)
                    targetIndex = Math.floor(Math.random()*targetDeckCopy.length)
                    targetCoord = targetDeckCopy[targetIndex]
                }

                //update player grid:
                const targetCell = playerGrid.findIndex(cell => cell.coord === targetCoord)
                const cellUpdate = {...playerGrid[targetCell]}
                cellUpdate.struck = true 
                const gridPayload = { targetCell, cellUpdate }
                dispatch(strikeCell(gridPayload))

                //update player ships:
                const occupant = cellUpdate.occupantID
                let shipTarget
                let sunk = false
                let totalSunk = 0
                if (occupant) {
                    const shipTargetIndex = playerShips.findIndex(ship => ship.type === occupant) 
                    shipTarget = playerShips[shipTargetIndex]          
                    sunk = shipTarget.health - 1 === 0
                    if (sunk) totalSunk++ //the state update doesn't immediately apply so I can't use Array.every to check if all ships have been sunk.
                    dispatch(reduceHealth(shipTargetIndex))
                }

                //update game status:
                const statusUpdate = occupant ? `hit ${formatShipTitle(shipTarget.type)}` : "miss"
                dispatch(assignCompStatus(statusUpdate))
                dispatch(incrementCompNotify())

                if (version === "Salvo" || version === "Revenge"){

                    if ((compShotsLeft - 1) === 0){
                        dispatch(toggleTurn())
                        dispatch(assignCompShots(maxCompSalvo))
                    } else {
                        dispatch(decrementCompShots())
                    }

                    setCompSalvoFired(prev => prev + 1)

                    if (sunk) {
                        let updatedPlayerSalvo
                        if (version === "Salvo"){
                            updatedPlayerSalvo = maxPlayerSalvo - 1
                            dispatch(decrementPlayerSalvo())
                        } else if (version === "Revenge"){
                            updatedPlayerSalvo = maxPlayerSalvo + 1
                            dispatch(incrementPlayerSalvo())
                        }
                        dispatch(assignPlayerShots(updatedPlayerSalvo))
                    }

                } else {
                    dispatch(toggleTurn())
                }

                //send alerts:
                if (sunk) {
                    playerShips.forEach(ship => {
                        if (!ship.health) totalSunk++
                    })
                    if (totalSunk === playerShips.length) {
                        dispatch(toggleGameOver())
                        //use timeout to make sure state updates get applied first
                        setTimeout(alertLose, 0)
                        return
                    } else {
                        setTimeout(() => {alertPlayerSunk(shipTarget.type)}, 0)
                    }
                }

                //remove targeted cells to prevent selecting them again:
                if (difficulty === "normal" || difficulty === "hard") {
                    if (occupant) {
                        if (sunk) { //remove target from honing deck
                            setHoningDeck(prev => prev.filter(target => target.type !== shipTarget.type))
                        } 
                        else if (shipTarget.type !== lastHit.current && !honingDeck.some(target => target.type === shipTarget.type)) { //if target wasn't previously hit then add it to the honing deck
                            const newHoningTarget = { type: shipTarget.type, orientation: undefined }
                            let nearbyCells = playerGrid.filter((cell, index) => {
                                if (!cell.struck && index !== targetCell) {
                                    if (cell.row === cellUpdate.row && (cell.column === cellUpdate.column + 1 || cell.column === cellUpdate.column - 1)) {
                                        return true
                                    } else if (cell.column === cellUpdate.column && (cell.row === cellUpdate.row + 1 || cell.row === cellUpdate.row - 1)) {
                                        return true
                                    }
                                }
                                return false
                            })
                            nearbyCells = nearbyCells.map(cell => {return { coord: cell.coord, row: cell.row, column: cell.column }}) //only take needed attributes
                            newHoningTarget.nearbyCells = nearbyCells

                            //if two ships are next to each other then the targeted cell needs to be removed from the last target's nearby cells:
                            const lastHoningTargetIndex = honingDeck.findIndex(target => target.type === lastHit.current)
                            let lastHoningTarget
                            if (lastHoningTargetIndex !== -1) {
                                lastHoningTarget = {...honingDeck[lastHoningTargetIndex]}
                                lastHoningTarget.nearbyCells.splice(targetIndex, 1)
                            }

                            setHoningDeck(prev => [...prev.map(target => {
                                    if (target.type === lastHit.current) return lastHoningTarget
                                    return target
                                }),
                                newHoningTarget
                            ])

                        } 
                        else { //if target was previously hit then its orientation is established and AI targeting becomes more focused
                            const honingUpdate = {...honingDeck.find(target => target.type === shipTarget.type)}
                            honingCellsCopy.splice(targetIndex, 1)
                            honingUpdate.nearbyCells = honingCellsCopy
                            if (!honingUpdate.orientation) {
                                honingUpdate.orientation = shipTarget.orientation
                                honingUpdate.nearbyCells = honingUpdate.nearbyCells.filter(cell => {
                                    if (honingUpdate.orientation === "landscape") {
                                        return cell.row === cellUpdate.row
                                    } else if (honingUpdate.orientation === "portrait") {
                                        return cell.column === cellUpdate.column
                                    }
                                    return false
                                })
                            }
                            let newCellTarget = playerGrid.find((cell, index) => {
                                if (!cell.struck && index !== targetCell) {
                                    if (honingUpdate.orientation === "landscape" && cell.row === cellUpdate.row && (cell.column === cellUpdate.column + 1 || cell.column === cellUpdate.column - 1)) {
                                        return true
                                    } else if (honingUpdate.orientation === "portrait" && cell.column === cellUpdate.column && (cell.row === cellUpdate.row + 1 || cell.row === cellUpdate.row - 1)) {
                                        return true
                                    }
                                }
                                return false
                            })
                            if (newCellTarget) {
                                newCellTarget = { coord: newCellTarget.coord, row: newCellTarget.row, column: newCellTarget.column }
                                honingUpdate.nearbyCells.push(newCellTarget)
                            }
                            setHoningDeck(prev => prev.map(target => {
                                if (target.type === honingUpdate.type) return honingUpdate
                                return target
                            }))
                        }
                        lastHit.current = shipTarget.type                     
                    } 
                    else if (!occupant && honingDeck.length) {
                        honingCellsCopy.splice(targetIndex, 1)
                        setHoningDeck(prev => prev.map((target, index) => {
                            if (index === 0) {
                                const targetUpdate = {...target}
                                targetUpdate.nearbyCells = honingCellsCopy
                                return targetUpdate
                            }
                            return target
                        }))
                    }
                }
                targetIndex = targetDeckCopy.findIndex(coord => coord === targetCoord) //make sure correct index is picked for the target deck
                targetDeckCopy.splice(targetIndex, 1)
                setTargetDeck(targetDeckCopy)
            }, 500)
        }
    // eslint-disable-next-line
    }, [playerTurn, compSalvoFired, dispatch])

    return (
        <div className="grid-wrapper">
            <div></div>
            <h2>Player Grid</h2>
            <div></div>
            <div className="column-labels">
                {setColLabels()}
            </div>
            <div className="row-labels">
                {rows.map(label => { return (
                    <div key={`player-row-${label}`}>{label}</div>
                )})}
            </div>
            <div id="player-grid" onDragOver={handleDragOver} onDrop={handleDrop}>
                {playerGrid
                    .map(cell => { return (
                        <div key={cell.coord} data-row={cell.row} data-column={cell.column}>
                            {cell.occupant &&
                                <div draggable={!started}
                                    className={`${!started ? "draggable" : ""} occupied ${cell.occupantID}--${cell.occupant.orientation}`}
                                    onDragStart={() => props.setDraggedShip(cell.occupant)}
                                    onMouseDown={(e) => props.setDragOrigin(parseInt(e.target.dataset.cell, 10))}
                                    onTouchStart={(e) => handleTouchStart(e, cell.occupant)}
                                    onTouchMove={handleTouchMove}
                                    onTouchEnd={handleTouchEnd}
                                >
                                    {buildShip(cell.occupant.size, true)}
                                </div>
                            }
                            {cell.struck &&
                                <div className={cell.occupantID ? "hit" : "miss"}></div>
                            }
                        </div>
                    )})
                }
            </div>
        </div>
    )
}

export default PlayerGrid