import React, { useRef, useEffect } from 'react'
import { useSelector } from 'react-redux'
import "../styles/game-status.scss"
import SalvoStatus from './gameStatus-sub-components/SalvoStatus'

const reanimateNotification = (element) => {
    element.classList.remove("squishy-notify");
    void element.offsetWidth; //triggers reflow to get animation to play
    element.classList.add("squishy-notify");
};

const GameStatus = () => {
    const gameOver = useSelector(state => state.gameOver)
    const playerTurn = useSelector(state => state.playerTurn)
    const playerStatus = useSelector(state => state.playerStatus)
    const computerStatus = useSelector(state => state.computerStatus)
    const playerNotifications = useSelector(state => state.playerNotifications)
    const compNotifications = useSelector(state => state.compNotifications)
    //game settings:
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)

    const playerLastTurn = useRef()
    const compLastTurn = useRef()

    useEffect(() => {
        reanimateNotification(playerLastTurn.current)
    }, [playerNotifications])

    useEffect(() => {
        reanimateNotification(compLastTurn.current)
    }, [compNotifications])

    return (
        <section className="game-status">
            <h3>Status</h3>
            <div style={{minWidth: "380px"}}>
                {!gameOver
                    ?   <>
                            <p>
                                <span style={{minWidth: "150px", display: "inline-block"}}>
                                    <span style={{fontWeight: "bold"}}>Turn: </span>
                                    {playerTurn ? "player" : "computer"}
                                </span>
                                {(difficulty === "hard") &&
                                    <>
                                        Player's shots fired: 
                                        <span style={{fontWeight: "bold"}}> {playerNotifications}/35</span>
                                    </>
                                }
                            </p>
                            {(version === "Salvo" || version === "Revenge") && <SalvoStatus/>}
                        </>
                    :   <p className="game-status__gameover">Game Over</p>
                }              
                <p>
                    <span style={{fontWeight: "bold"}}>Player's last turn: </span>
                    <span className="game-status__last-turn" ref={playerLastTurn}>{playerStatus}</span>
                </p>
                <p>
                    <span style={{fontWeight: "bold"}}>Computer's last turn: </span>
                    <span className="game-status__last-turn" ref={compLastTurn}>{computerStatus}</span>                
                </p>
            </div>
        </section>
    )
}

export default GameStatus