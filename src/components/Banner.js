import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { toggleStarted } from '../state/game-status/startedSlice'
//Components:
import RulesModal from './banner-sub-components/RulesModal'
import SettingsModal from './banner-sub-components/SettingsModal'
import Tooltip from '@mui/material/Tooltip'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGear } from '@fortawesome/free-solid-svg-icons'
//Styles:
import "../styles/banner.scss"
import "../styles/modals.scss"
//Assets:
import BkgRipples from "../assets/background-ripples.jpg"

const Banner = () => {
    const [orientation, setOrientation] = useState((window.innerHeight > window.innerWidth) ? "portrait" : "landscape");
    const [bannerOpen, setBannerOpen] = useState(true)
    const [rulesOpen, setRulesOpen] = useState(false)
    const [settingsOpen, setSettingsOpen] = useState(false)
    //game status:
    const started = useSelector(state => state.started)
    //game settings:
    const notifyShipHit = useSelector(state => state.notifyShipHit)
    const alertSunk = useSelector(state => state.alertSunk)
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)
    //player state:
    const playerShips = useSelector(state => state.playerShips)

    const dispatch = useDispatch()

    const settings = useRef()

    const clearMenuAnimation = () => {
        settings.current.classList.remove("spin-close")
        settings.current.classList.remove("spin-open")
    }

    const toggleMenu = () => {
        clearMenuAnimation()
        if (bannerOpen) {   
            settings.current.classList.add("spin-close")
        } else {     
            settings.current.classList.add("spin-open")
        }
        setBannerOpen(!bannerOpen)
    }

    useEffect(() => {
        const screenType = window.matchMedia("(orientation: portrait)")

        function changeOrientation(e){
            const orientation = e.matches ? "portrait" : "landscape"
            clearMenuAnimation()
            setOrientation(orientation)
        }

        screenType.addEventListener("change", changeOrientation)

        return () => {
            screenType.removeEventListener("change", changeOrientation)
        }
    }, [])

    return (
        <>
            <div className={`menu-toggle ${(orientation === "landscape") ? "hidden" : ""}`} >
                <span className={"menu-toggle__gear"} ref={settings} onClick={toggleMenu}>
                    <FontAwesomeIcon icon={faGear} />
                </span>
                <p className={"menu-toggle__note"}>Open/Close Menu</p>
            </div>
            <aside className={(orientation === "portrait" && !bannerOpen) ? "invisible ghost" : ""} >
                <div id="aside-wrapper">
                    <div id="banner-bkg-img-wrapper">
                        <img src={BkgRipples} alt="Background of ocean ripples on a beach."></img>
                    </div>
                    <div id="banner-content-wrapper">
                        <h1>Battleship</h1>
                        <div id="banner-btns-wrapper">
                            <button type="button" id="btn-rules" onClick={() => setRulesOpen(true)}>Rules</button>
                            <button type="button" id="btn-settings" onClick={() => setSettingsOpen(true)}>Settings</button>
                            {started
                                ?   <button type="button" id="btn-restart" onClick={() => window.location.reload()}>Restart</button>
                                :
                                    <Tooltip
                                        arrow placement="right"
                                        title={<div id="prestart-txt">Place all of your ships before <br/> you can start the game</div>}
                                        disableFocusListener={playerShips.every(ship => ship.deployed)}
                                        disableHoverListener={playerShips.every(ship => ship.deployed)}
                                        disableTouchListener={playerShips.every(ship => ship.deployed)}
                                    >
                                        <div>
                                            <button type="button" id="btn-start"
                                                disabled={playerShips.some(ship => !ship.deployed)}
                                                onClick={() => dispatch(toggleStarted())}
                                            >
                                                Start Game
                                            </button>
                                        </div>
                                    </Tooltip>
                            }
                        </div>
                        <div className="current-settings">
                            <h3>Current Settings</h3>
                            <p>
                                <span className="current-settings__name">Notify enemy ship hit:</span>
                                <br/>
                                <span className="current-settings__value">{notifyShipHit ? "on" : "off"}</span>
                            </p>
                            <p>
                                <span className="current-settings__name">Alert enemy ship sunk:</span>
                                <br/>
                                <span className="current-settings__value">{alertSunk ? "on" : "off"}</span>
                            </p>
                            <p>
                                <span className="current-settings__name">Difficulty:</span>
                                <br/>
                                <span className="current-settings__value">{difficulty}</span>
                            </p>
                            <p>
                                <span className="current-settings__name">Version:</span>
                                <br/>
                                <span className="current-settings__value">{version}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </aside>
            <RulesModal rulesOpen={rulesOpen} setRulesOpen={setRulesOpen} />
            <SettingsModal settingsOpen={settingsOpen} setSettingsOpen={setSettingsOpen} />
        </>
    )
}

export default Banner