import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { assignNotifyHit } from '../../state/game-settings/notifyHitSlice'
import { assignAlertSunk } from '../../state/game-settings/alertSunkSlice'
import { assignDifficulty } from '../../state/game-settings/difficultySlice'
import { assignVersion } from '../../state/game-settings/versionSlice'

import Modal from '@mui/material/Modal'
import Swal from 'sweetalert2'
import "../../styles/settings.scss"

const SettingsModal = (props) => {
    //game status:
    const started = useSelector(state => state.started)
    //game settings:
    const notifyShipHit = useSelector(state => state.notifyShipHit)
    const alertSunk = useSelector(state => state.alertSunk)
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)
    
    const dispatch = useDispatch()

    const handleSubmit = (e) => {
        e.preventDefault()
        const inputs = e.target.elements
        dispatch(assignNotifyHit(inputs["notify-ship-hit"].checked))
        dispatch(assignAlertSunk(inputs["notify-ship-sunk"].checked))
        dispatch(assignDifficulty(inputs["battleship-difficulty"].value))
        dispatch(assignVersion(inputs["battleship-version"].value))

        Swal.fire({
            icon: "success",
            title: "Game settings have been successfully updated.",
            timer: 2000,
            customClass: {
                container: "alert-settings",
                title: "alert-settings",
                confirmButton: "alert-settings"
            }
        })
    }

    return (
        <Modal
            open={props.settingsOpen}
            onClose={() => props.setSettingsOpen(false)}
            aria-labelledby="settings-modal"
            aria-describedby="Game settings"
        >
            <div className="modal">
                <h3>Game Settings</h3>
                <div className="modal__content-wrapper">
                    <p style={{textAlign: "center"}}>NOTE: once the game has been started the settings cannot be changed.</p>
                    <form id="game-settings-form" className="game-settings" onSubmit={handleSubmit}>
                        <h4>Notifications</h4>
                        <p style={{margin: "10px 0"}}>For more of a challenge you can disable some notifications:</p>
                        <div className="game-settings__notifications">
                            <input type="checkbox" name="notify-ship-hit" id="cb-hit" defaultChecked={notifyShipHit} />
                            <label htmlFor="cb-hit">Notify which enemy ship you hit.</label>
                            <input type="checkbox" name="notify-ship-sunk" id="cb-sunk" defaultChecked={alertSunk} />
                            <label htmlFor="cb-sunk">Alert when you sink an enemy ship.</label>
                        </div>
                        <h4>Difficulty</h4>
                        <div className="game-settings__difficulty">
                            <input type="radio" name="battleship-difficulty" value="easy" id="radio-easy" defaultChecked={difficulty === "easy"}/>
                            <label htmlFor="radio-easy">Easy</label>
                            <p>- enemy AI randomly targets cells.</p>
                            <input type="radio" name="battleship-difficulty" value="normal" id="radio-normal" defaultChecked={difficulty === "normal"}/>
                            <label htmlFor="radio-normal">Normal</label>
                            <p>- when enemy AI strikes a ship it will hone in on nearby cells.</p>
                            <input type="radio" name="battleship-difficulty" value="hard" id="radio-hard" defaultChecked={difficulty === "hard"}/>
                            <label htmlFor="radio-hard">Hard</label>
                            <p>- you have a limited supply of missiles (35) and must win before you run out of ammo. (Enemy AI will continue to hone in on nearby cells after it strikes a ship).</p>
                        </div>
                        <h4>Version</h4>
                        <div className="game-settings__version">
                            <input type="radio" name="battleship-version" value="Classic" id="radio-classic" defaultChecked={version === "Classic"}/>
                            <label htmlFor="radio-classic">Classic</label>
                            <input type="radio" name="battleship-version" value="Salvo" id="radio-salvo" defaultChecked={version === "Salvo"}/>
                            <label htmlFor="radio-salvo">Salvo</label>
                            <input type="radio" name="battleship-version" value="Revenge" id="radio-revenge" defaultChecked={version === "Revenge"}/>
                            <label htmlFor="radio-revenge">Revenge</label>
                        </div>
                    </form>
                </div>
                <div className="modal__btn-wrapper">
                    {!started &&
                        <button type="submit" form="game-settings-form" id="btn-apply-settings">Apply</button>
                    }
                    <button type="button" className="modal-close-btn" onClick={() => props.setSettingsOpen(false)}>Close</button>
                </div>
            </div>
        </Modal>
    )
}

export default SettingsModal