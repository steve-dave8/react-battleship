import React from 'react'
import Modal from '@mui/material/Modal'

const RulesModal = (props) => {
    return (
        <Modal
            open={props.rulesOpen}
            onClose={() => props.setRulesOpen(false)}
            aria-labelledby="rules-modal"
            aria-describedby="Rules of the Battleship game"
        >
            <div className="modal">
                <h3>Rules of the Game</h3>
                <div className="modal__content-wrapper">
                    <h4>Object</h4>
                    <p>Sink your opponent's fleet of four ships before your opponent sinks yours.</p>

                    <h4>Rules for ship placement</h4>
                    <ul>
                        <li>Ships cannot be placed diagonally.</li>
                        <li>Ships cannot be placed such that a part of the ship would be off of the grid.</li>
                        <li>Ships cannot be placed along a grid gap.</li>
                        <li>Once the game has started, ships cannot be repositioned.</li>
                    </ul>

                    <h4>The classic game</h4>
                    <p>Turns are alternated, firing one shot per turn to try to hit each other's ships.</p>
                    <h5>Call your shot!</h5>
                    <p>
                        On your turn, click a coordinate on the computer grid. If it's a hit, the coordinate will get a red marker. If it's a miss then it will get a white marker. Shots fired on the player grid will be marked similarly. 
                        When you strike one of the computer's ships, you will be alerted as to which ship was hit. Coordinates that have been struck cannot be targeted again.
                    </p>
                    <h5>Sinking a ship</h5>
                    <p>Once all cells in a ship have red markers, it has been sunk. When you sink one of the computer's ships, you will be alerted as to which ship was sunk.</p>
                    
                    <h4>The salvo game</h4>
                    <p>
                        Experienced players may enjoy this game variation, in which you get as many shots on your turn as ships afloat in your fleet. 
                        The rules are the same except for the following:
                    </p>
                    <ul>
                        <li>You start with a salvo of four shots (one for each ship in your fleet) and fire them one at a time. Hits and misses are marked the same as for the standard game.</li>
                        <li>For each ship that is sunk, you lose one shot in the salvo on your next turn.</li>
                    </ul>
                    <p>Continue calling out salvos until one player wins.</p>

                    <h4>The revenge game</h4>
                    <p>
                        Similar to the Salvo variation except players start off with one shot each and for each ship that is sunk you gain one shot in the salvo on your next turn. 
                        It adds a different element of strategy and improves the chances of making a comeback to win the game.
                    </p>
                    
                    <h4>Winning the game</h4>
                    <p>If you sink your opponent's fleet of four ships before they sinks yours, you win the game!</p>
                </div>
                <div className="modal__btn-wrapper">
                    <button type="button" className="modal-close-btn" onClick={() => props.setRulesOpen(false)}>Close</button>
                </div>
            </div>
        </Modal>
    )
}

export default RulesModal