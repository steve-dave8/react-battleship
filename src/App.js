import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { assignPlayerSalvo } from './state/salvo/maxPlayerSalvoSlice'
import { assignPlayerShots } from './state/salvo/playerShotsLeftSlice'
import { assignCompSalvo } from './state/salvo/maxCompSalvoSlice'
import { assignCompShots } from './state/salvo/compShotsLeftSlice'
//Component imports:
import Banner from './components/Banner'
import GameBoard from './components/GameBoard'
import Battleships from './components/Battleships'
import GameStatus from './components/GameStatus'
//Asset imports:
import { fleet } from "./assets/fleet"

function App() {
    const dispatch = useDispatch()

    //game status:
    const started = useSelector(state => state.started)
    //game settings:
    const notifyShipHit = useSelector(state => state.notifyShipHit)
    const alertSunk = useSelector(state => state.alertSunk)
    const difficulty = useSelector(state => state.difficulty)
    const version = useSelector(state => state.version)
    //for player ships:
    const [draggedShip, setDraggedShip] = useState({})
    const [dragOrigin, setDragOrigin] = useState(0)
    const [touchMoving, setTouchMoving] = useState(false)
    const [touchDropTarget, setTouchDropTarget] = useState({})

    //remember game settings by putting them in local storage:
    useEffect(() => {
        window.localStorage.setItem('battleship-notifyShipHit', JSON.stringify(notifyShipHit))
    }, [notifyShipHit])

    useEffect(() => {
        window.localStorage.setItem('battleship-alertSunk', JSON.stringify(alertSunk))
    }, [alertSunk])

    useEffect(() => {
        window.localStorage.setItem('battleship-difficulty', difficulty)
    }, [difficulty])

    useEffect(() => {
        window.localStorage.setItem('battleship-version', version)
    }, [version])

    //set initial salvo based on game version:
    useEffect(() => {
        if (version === "Salvo"){
            dispatch(assignPlayerSalvo(fleet.length))
            dispatch(assignPlayerShots(fleet.length))
            dispatch(assignCompSalvo(fleet.length))
            dispatch(assignCompShots(fleet.length))
        }
        if (version === "Revenge"){
            dispatch(assignPlayerSalvo(1))
            dispatch(assignPlayerShots(1))
            dispatch(assignCompSalvo(1))
            dispatch(assignCompShots(1))
        }
    }, [version, dispatch])

    return (
        <>
            <Banner/>
            <main className={!started ? "no-scroll" : ""}>
                <GameBoard
                    draggedShip={draggedShip}
                    setDraggedShip={setDraggedShip}
                    dragOrigin={dragOrigin}
                    setDragOrigin={setDragOrigin}
                    touchMoving={touchMoving}
                    setTouchMoving={setTouchMoving}
                    touchDropTarget={touchDropTarget}
                    setTouchDropTarget={setTouchDropTarget}
                />
                {!started
                    ?   <Battleships
                            draggedShip={draggedShip}
                            setDraggedShip={setDraggedShip}
                            setDragOrigin={setDragOrigin}
                            touchMoving={touchMoving}
                            setTouchMoving={setTouchMoving}
                            touchDropTarget={touchDropTarget}
                            setTouchDropTarget={setTouchDropTarget}
                        />
                    :   <GameStatus/>
                }
            </main>
        </>
    )
}

export default App