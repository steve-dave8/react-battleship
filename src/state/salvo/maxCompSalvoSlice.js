import { createSlice } from '@reduxjs/toolkit';

const mcSalvoSlice = createSlice({
    name: 'maxCompSalvo',
    initialState: 0,
    reducers: {
        incrementCompSalvo: state => {
            return state + 1
        },
        decrementCompSalvo: state => {
            return state - 1
        },
        assignCompSalvo(state, action) {
            return action.payload
        }
    }
});

export const { incrementCompSalvo, decrementCompSalvo, assignCompSalvo } = mcSalvoSlice.actions;

export default mcSalvoSlice.reducer;