import { createSlice } from '@reduxjs/toolkit';

const cShotsSlice = createSlice({
    name: 'compShotsLeft',
    initialState: 0,
    reducers: {
        decrementCompShots: state => {
            return state - 1
        },
        assignCompShots(state, action) {
            return action.payload
        }
    }
});

export const { decrementCompShots, assignCompShots } = cShotsSlice.actions;

export default cShotsSlice.reducer;