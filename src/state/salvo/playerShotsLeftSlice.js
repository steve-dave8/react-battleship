import { createSlice } from '@reduxjs/toolkit';

const pShotsSlice = createSlice({
    name: 'playerShotsLeft',
    initialState: 0,
    reducers: {
        decrementPlayerShots: state => {
            return state - 1;
        },
        assignPlayerShots(state, action) {
            return action.payload;
        }
    }
});

export const { decrementPlayerShots, assignPlayerShots } = pShotsSlice.actions;

export default pShotsSlice.reducer;