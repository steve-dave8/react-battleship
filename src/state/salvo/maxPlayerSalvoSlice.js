import { createSlice } from '@reduxjs/toolkit';

const mpSalvoSlice = createSlice({
    name: 'maxPlayerSalvo',
    initialState: 0,
    reducers: {
        incrementPlayerSalvo: state => {
            return state + 1
        },
        decrementPlayerSalvo: state => {
            return state - 1
        },
        assignPlayerSalvo(state, action) {
            return action.payload
        }
    }
});

export const { incrementPlayerSalvo, decrementPlayerSalvo, assignPlayerSalvo } = mpSalvoSlice.actions;

export default mpSalvoSlice.reducer;