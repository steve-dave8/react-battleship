import { createSlice } from '@reduxjs/toolkit';

/*Player notifications are numbers used to increment to trigger the animation for the notification text.
They are also used for the hard difficulty.*/

const pNotifySlice = createSlice({
    name: 'playerNotifications',
    initialState: 0,
    reducers: {
        incrementPlayerNotify: state => {
            return state + 1
        }
    }
});

export const { incrementPlayerNotify } = pNotifySlice.actions;

export default pNotifySlice.reducer;