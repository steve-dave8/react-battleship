import { createSlice } from '@reduxjs/toolkit';

const startedSlice = createSlice({
    name: 'started',
    initialState: false,
    reducers: {
        toggleStarted: state => {
            return !state;
        }
    }
});

export const { toggleStarted } = startedSlice.actions;

export default startedSlice.reducer;