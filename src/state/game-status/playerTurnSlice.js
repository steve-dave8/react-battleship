import { createSlice } from '@reduxjs/toolkit';

const pTurnSlice = createSlice({
    name: 'playerTurn',
    initialState: true,
    reducers: {
        toggleTurn: state => {
            return !state;
        }
    }
});

export const { toggleTurn } = pTurnSlice.actions;

export default pTurnSlice.reducer;