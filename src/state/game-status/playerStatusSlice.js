import { createSlice } from '@reduxjs/toolkit';

const playerStatusSlice = createSlice({
    name: 'playerStatus',
    initialState: 'preparing missiles...',
    reducers: {
        assignPlayerStatus(state, action) {
            return action.payload
        }
    }
});

export const { assignPlayerStatus } = playerStatusSlice.actions;

export default playerStatusSlice.reducer;