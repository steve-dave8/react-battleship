import { createSlice } from '@reduxjs/toolkit';

const gameOverSlice = createSlice({
    name: 'gameOver',
    initialState: false,
    reducers: {
        toggleGameOver: state => {
            return !state;
        }
    }
});

export const { toggleGameOver } = gameOverSlice.actions;

export default gameOverSlice.reducer;