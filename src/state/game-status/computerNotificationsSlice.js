import { createSlice } from '@reduxjs/toolkit';

//Computer notifications are arbitrary numbers used to increment to trigger the animation for the notification text.

const cNotifySlice = createSlice({
    name: 'compNotifications',
    initialState: 0,
    reducers: {
        incrementCompNotify: state => {
            return state + 1
        }
    }
});

export const { incrementCompNotify } = cNotifySlice.actions;

export default cNotifySlice.reducer;