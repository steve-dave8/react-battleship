import { createSlice } from '@reduxjs/toolkit';

const compStatusSlice = createSlice({
    name: 'computerStatus',
    initialState: 'preparing missiles...',
    reducers: {
        assignCompStatus(state, action) {
            return action.payload
        }
    }
});

export const { assignCompStatus } = compStatusSlice.actions;

export default compStatusSlice.reducer;