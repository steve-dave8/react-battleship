import { createSlice } from '@reduxjs/toolkit';
import { fleet } from '../../assets/fleet';

const pShipsSlice = createSlice({
    name: 'playerShips',
    initialState: fleet,
    reducers: {
        rotateAllShips: state => {
            state.forEach(ship => {
                if (!ship.deployed) {
                    ship.orientation = ship.orientation === "landscape" ? "portrait" : "landscape"
                }
            })
        },
        rotateOneShip: (state, action) => {
            const shipType = action.payload
            const selectedShip = state.find(ship => ship.type === shipType)
            selectedShip.orientation = selectedShip.orientation === "landscape" ? "portrait" : "landscape"
        },
        returnShip: (state, action) => {
            const shipType = action.payload
            const selectedShip = state.find(ship => ship.type === shipType)
            selectedShip.deployed = false
        },
        deployShip: (state, action) => {
            const shipType = action.payload
            const selectedShip = state.find(ship => ship.type === shipType)
            selectedShip.deployed = true
        },
        reduceHealth: (state, action) => {
            const index = action.payload
            state[index].health -= 1
        }
    }
});

export const { rotateAllShips, rotateOneShip, returnShip, deployShip, reduceHealth } = pShipsSlice.actions;

export default pShipsSlice.reducer;