import { createSlice } from '@reduxjs/toolkit';
import { oceanGrid } from '../../assets/ocean-grid';

const pGridSlice = createSlice({
    name: 'playerGrid',
    initialState: oceanGrid,
    reducers: {
        removeShipFromGrid: (state, action) => {
            const shipType = action.payload
            return state.map(cell => {
                if (cell.occupantID === shipType) {
                    return {
                        ...cell,
                        occupantID: null,
                        occupant: null
                    }
                }
                return cell
            })
        },
        addShipToGrid: (state, action) => {
            const { targetRows, targetCols, draggedShip } = action.payload
            return state.map(cell => {
                if (cell.column === targetCols[0] && cell.row === targetRows[0]) {
                    return {
                        ...cell,
                        occupant: draggedShip,
                        occupantID: draggedShip.type
                    }
                } else if (targetRows.includes(cell.row) && targetCols.includes(cell.column)) {
                    return {
                        ...cell,
                        occupantID: draggedShip.type
                    }
                //prevent duplication if ship was placed on the grid:
                } else if (cell.occupantID === draggedShip.type) {
                    return {
                        ...cell,
                        occupant: null,
                        occupantID: null
                    }
                }
                return cell
            })
        },
        strikeCell: (state, action) => {
            const { targetCell, cellUpdate } = action.payload
            state[targetCell] = cellUpdate
        }
    }
});

export const { removeShipFromGrid, addShipToGrid, strikeCell } = pGridSlice.actions;

export default pGridSlice.reducer;