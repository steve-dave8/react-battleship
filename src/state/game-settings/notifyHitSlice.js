import { createSlice } from '@reduxjs/toolkit';

const prevNotifyShipHit = window.localStorage.getItem('battleship-notifyShipHit');

const initialState = prevNotifyShipHit ? JSON.parse(prevNotifyShipHit) : true ;

const notifyHitSlice = createSlice({
    name: 'notifyShipHit',
    initialState,
    reducers: {
        assignNotifyHit(state, action) {
            return action.payload;
        }
    }
});

export const { assignNotifyHit } = notifyHitSlice.actions;

export default notifyHitSlice.reducer;