import { createSlice } from '@reduxjs/toolkit';

const prevDifficulty = window.localStorage.getItem('battleship-difficulty');

const initialState = prevDifficulty || "easy" ;

const difficultySlice = createSlice({
    name: 'difficulty',
    initialState,
    reducers: {
        assignDifficulty(state, action) {
            return action.payload;
        }
    }
});

export const { assignDifficulty } = difficultySlice.actions;

export default difficultySlice.reducer;