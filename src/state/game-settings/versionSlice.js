import { createSlice } from '@reduxjs/toolkit';

const prevVersion = window.localStorage.getItem('battleship-version');

const initialState = prevVersion || "Classic" ;

const versionSlice = createSlice({
    name: 'version',
    initialState,
    reducers: {
        assignVersion(state, action) {
            return action.payload;
        }
    }
});

export const { assignVersion } = versionSlice.actions;

export default versionSlice.reducer;