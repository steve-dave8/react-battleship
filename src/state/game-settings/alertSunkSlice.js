import { createSlice } from '@reduxjs/toolkit';

const prevAlertSunk = window.localStorage.getItem('battleship-alertSunk');

const initialState = prevAlertSunk ? JSON.parse(prevAlertSunk) : true ;

const alertSunkSlice = createSlice({
    name: 'alertSunk',
    initialState,
    reducers: {
        assignAlertSunk(state, action) {
            return action.payload;
        }
    }
});

export const { assignAlertSunk } = alertSunkSlice.actions;

export default alertSunkSlice.reducer;