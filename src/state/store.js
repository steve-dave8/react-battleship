import { configureStore } from '@reduxjs/toolkit'

import startedReducer from './game-status/startedSlice'
import gameOverReducer from './game-status/gameOverSlice'
import playerTurnReducer from './game-status/playerTurnSlice'
import playerStatusReducer from './game-status/playerStatusSlice'
import computerStatusReducer from './game-status/compStatusSlice'
import pNotifyReducer from './game-status/playerNotificationsSlice'
import cNotifyReducer from './game-status/computerNotificationsSlice'

import notifyHitReducer from './game-settings/notifyHitSlice'
import alertSunkReducer from './game-settings/alertSunkSlice'
import difficultyReducer from './game-settings/difficultySlice'
import versionReducer from './game-settings/versionSlice'

import playerGridReducer from './player/playerGridSlice'
import pShipsReducer from './player/playerShipsSlice'

import maxPlayerSalvoReducer from './salvo/maxPlayerSalvoSlice'
import playerShotsReducer from './salvo/playerShotsLeftSlice'
import maxCompSalvoReducer from './salvo/maxCompSalvoSlice'
import compShotsReducer from './salvo/compShotsLeftSlice'

export default configureStore({
  reducer: {
    started: startedReducer,
    gameOver: gameOverReducer,
    playerTurn: playerTurnReducer,
    playerStatus: playerStatusReducer,
    computerStatus: computerStatusReducer,
    playerNotifications: pNotifyReducer,
    compNotifications: cNotifyReducer,
    notifyShipHit: notifyHitReducer,
    alertSunk: alertSunkReducer,
    difficulty: difficultyReducer,
    version: versionReducer,
    playerGrid: playerGridReducer,
    playerShips: pShipsReducer,
    maxPlayerSalvo: maxPlayerSalvoReducer,
    playerShotsLeft: playerShotsReducer,
    maxCompSalvo: maxCompSalvoReducer,
    compShotsLeft: compShotsReducer
  }
})