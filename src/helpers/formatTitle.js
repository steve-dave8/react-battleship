const formatShipTitle = (string) => {
    let shipTitle = string.split('-').map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(' ');
    return shipTitle;
};

export { formatShipTitle };