# React Battleship

Live website: <http://battleship.stevenwhite.me/>

## Description

This is an electronic version of the classic Battleship game where you play against the computer in a race to sink each other's ships. It is a client-side, single-page application made with ReactJS. Other main technologies used include SASS and [HTML5 Drag and Drop API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API). Note: the game is for desktop and tablet only.

### Features

* for an extra challenge, the player can turn off specific notifications as to which enemy ship was hit or sunk

* difficulty levels: easy, normal, hard

* game variations: Classic, Salvo, Revenge

## Environments

### Local Dev Environment

After cloning this repo, in your terminal (while in the relevant folder) enter the following commands to run the app locally:

`npm install`

`npm start`

### Deployment

Continuous deployment has been set up for this project using GitLab CI/CD variables. When changes are pushed/merged to the main branch they will be automatically deployed.